const protobuf = require("protobufjs");
/**
 *
 * @returns {Promise<any>}
 */
exports.loadPhotoProto = () => {
  return new Promise((resolve, reject) => {
    protobuf.load("./protobuf-models/photo.proto", function (err, root) {
      if (err) {
        return reject(err);
      }
      return resolve(root);
    });
  });
};

/**
 *
 * @returns {Promise<any>}
 */
exports.loadSensorDataProto = () => {
  return new Promise((resolve, reject) => {
    protobuf.load("./protobuf-models/sensordata.proto", function (err, root) {
      if (err) {
        return reject(err);
      }
      return resolve(root);
    });
  });
};

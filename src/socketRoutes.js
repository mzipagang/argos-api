const config = require('./config');
const cookie = require('cookie');
/**
 * Socket.io "routes" (events and event handlers)
 */
const SocketIoClient = require('socket.io-client');

'use strict';

module.exports = (io, fastify) => {
    io.use((socket, next) => {
        if (socket.handshake.headers && socket.handshake.headers.cookie) {
            const cookieObj = cookie.parse(socket.handshake.headers.cookie);
            try {
                if (cookieObj.token) {
                    fastify.jwt.verify(cookieObj.token, config.super_secret, (err, decoded) => {
                        if (err) {
                            console.log('Socket auth error: invalid token');
                            return next(new Error('Authentication error'));
                        }
                        socket.decoded = decoded;
                        next();
                    });
                } else {
                    console.log('Socket auth error: missing token' );
                    next(new Error('Authentication error'));
                }
            } catch (e) {
                console.log('Socket auth error: ', e);
                next(new Error('Authentication error'));
            }
        } else {
            console.log('Socket auth error: no cookie');
            next(new Error('Authentication error'));
        }
    }).on('connection', socket => {
        // Connection now authenticated to receive further events
        console.log('client socket connected');
        const socketEvent = socket.handshake.query && socket.handshake.query.event;
        [
          './api/v1/photo/index.socket.js',
          './api/v1/sensordata/index.socket.js'
        ].forEach((path) => {
          require(path).forEach((route) => {
            if (socketEvent && route.event === socketEvent) {
              console.log(`event ${socketEvent} registered for socket: ${socket.id}`);
              route.subscribe(socket);
              socket.on('disconnect', () => {
                if (route.unsubscribe) {
                  route.unsubscribe(socket);
                }
                console.log('socket disconnected');
              });
            }
          });
        });
    });
};

'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('OperatorRole', [
            {
              operatorId: 1,
              roleId: 1
            },
            {
              operatorId: 2,
              roleId: 1
            },
            {
              operatorId: 3,
              roleId: 1
            },
            {
              operatorId: 4,
              roleId: 1
            },
            {
              operatorId: 5,
              roleId: 1
            },
            {
              operatorId: 6,
              roleId: 1
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('OperatorRole', null, {});

    }
};

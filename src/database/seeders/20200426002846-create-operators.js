'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Operator', [
            {
                id: 1,
                email: 'test@panoptic.com',
                username: 'superadmin',
                passwordHash: 'FpPQ4pixP1/O+ZWYsEVzJ0g/krbBjx0xoj+Xig02g6NFNdp3XMbZejSh5aL1kNQGGFOdPcUFnTA4cK3yz0Ks6w==',
                salt: 'egd5JYwhVUDOEb5FX1I1VA==',
                title: 'Mr.',
                firstName: 'Super',
                lastName: 'Admin',
                createdAt: '2020-04-20 21:38:53',
                createdBy: 'mzipagang@boldtype.com',
                updatedAt: '2020-04-20 21:38:53',
                updatedBy: 'mzipagang@boldtype.com'
            },
            {
                id: 2,
                email: 'mzipagang@panoptic.com',
                username: 'mzipagang',
                passwordHash: 'FpPQ4pixP1/O+ZWYsEVzJ0g/krbBjx0xoj+Xig02g6NFNdp3XMbZejSh5aL1kNQGGFOdPcUFnTA4cK3yz0Ks6w==',
                salt: 'egd5JYwhVUDOEb5FX1I1VA==',
                title: 'Mr.',
                firstName: 'Mark',
                lastName: 'Zipagang',
                createdAt: '2020-04-20 21:38:53',
                createdBy: 'mzipagang@panoptic.com',
                updatedAt: '2020-04-20 21:38:53',
                updatedBy: 'mzipagang@panoptic.com'
            },
            {
                id: 3,
                email: 'earriaga@panoptic.com',
                username: 'earriaga',
                passwordHash: 'FpPQ4pixP1/O+ZWYsEVzJ0g/krbBjx0xoj+Xig02g6NFNdp3XMbZejSh5aL1kNQGGFOdPcUFnTA4cK3yz0Ks6w==',
                salt: 'egd5JYwhVUDOEb5FX1I1VA==',
                title: 'Mr.',
                firstName: 'Eddy',
                lastName: 'Arriaga',
                createdAt: '2020-04-20 21:38:53',
                createdBy: 'mzipagang@panoptic.com',
                updatedAt: '2020-04-20 21:38:53',
                updatedBy: 'mzipagang@panoptic.com'
            },
            {
                id: 4,
                email: 'jchin@panoptic.com',
                username: 'jchin',
                passwordHash: 'FpPQ4pixP1/O+ZWYsEVzJ0g/krbBjx0xoj+Xig02g6NFNdp3XMbZejSh5aL1kNQGGFOdPcUFnTA4cK3yz0Ks6w==',
                salt: 'egd5JYwhVUDOEb5FX1I1VA==',
                title: 'Mr.',
                firstName: 'Joshua',
                lastName: 'Chin',
                createdAt: '2020-04-20 21:38:53',
                createdBy: 'mzipagang@panoptic.com',
                updatedAt: '2020-04-20 21:38:53',
                updatedBy: 'mzipagang@panoptic.com'
            },
            {
                id: 5,
                email: 'jzipagang@panoptic.com',
                username: 'jzipagang',
                passwordHash: 'FpPQ4pixP1/O+ZWYsEVzJ0g/krbBjx0xoj+Xig02g6NFNdp3XMbZejSh5aL1kNQGGFOdPcUFnTA4cK3yz0Ks6w==',
                salt: 'egd5JYwhVUDOEb5FX1I1VA==',
                title: 'Mrs.',
                firstName: 'Janelle',
                lastName: 'Zipagang',
                createdAt: '2020-04-20 21:38:53',
                createdBy: 'mzipagang@panoptic.com',
                updatedAt: '2020-04-20 21:38:53',
                updatedBy: 'mzipagang@panoptic.com'
            },
            {
                id: 6,
                email: 'alakshmipathy@panoptic.com',
                username: 'alakshmipathy',
                passwordHash: 'FpPQ4pixP1/O+ZWYsEVzJ0g/krbBjx0xoj+Xig02g6NFNdp3XMbZejSh5aL1kNQGGFOdPcUFnTA4cK3yz0Ks6w==',
                salt: 'egd5JYwhVUDOEb5FX1I1VA==',
                title: 'Mrs.',
                firstName: 'Alexandra',
                lastName: 'Lakshmipathy',
                createdAt: '2020-04-20 21:38:53',
                createdBy: 'mzipagang@panoptic.com',
                updatedAt: '2020-04-20 21:38:53',
                updatedBy: 'mzipagang@panoptic.com'
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Operator', null, {});
    }
};

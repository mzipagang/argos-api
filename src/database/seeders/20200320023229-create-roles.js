'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Role', [
            {
              id: 1,
              name: 'Admin'
            },
            {
              id: 2,
              name: 'Technician'
            },
            {
              id: 3,
              name: 'Field'
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Role', null, {});

    }
};

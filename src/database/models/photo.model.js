'use strict';
module.exports = (sequelize, DataTypes) => {
  const Photo = sequelize.define('Photo', {
    deviceId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    timestamp: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    result: {
      type: DataTypes.JSONB
    }
  },
  {
    freezeTableName: true,
    timestamps: false
  }, {});
  return Photo;
};

'use strict';

module.exports = function (sequelize, DataTypes) {
    const Device = sequelize.define('Device', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        serial: DataTypes.INTEGER,
        lat: DataTypes.DOUBLE,
        lng: DataTypes.DOUBLE,
        status: DataTypes.STRING,
        createdAt: DataTypes.DATE,
        createdBy: DataTypes.STRING,
        updatedAt: DataTypes.DATE,
        updatedBy: DataTypes.STRING,
        deletedAt: DataTypes.DATE,
        deletedBy: DataTypes.STRING
    },
    {
        freezeTableName: true,
        paranoid: true
    },
    {
        indexes: [{
            fields: ['serial'],
            primary: false,
            unique: false
        }]
    });
    Device.associate = function({ SiteDevice }) {
        Device.hasOne(SiteDevice, {sourceKey: 'id', foreignKey: 'deviceId'});
    };
    return Device;
};

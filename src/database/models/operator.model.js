'use strict';

module.exports = function (sequelize, DataTypes) {
  const Operator = sequelize.define('Operator', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    passwordHash: DataTypes.STRING,
    salt: DataTypes.STRING,
    previousPasswords: DataTypes.JSONB,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    title: DataTypes.STRING,
    phone: DataTypes.STRING,
    mobile: DataTypes.STRING,
    fax: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    createdBy: DataTypes.STRING,
    updatedAt: DataTypes.DATE,
    updatedBy: DataTypes.STRING,
    deletedAt: DataTypes.DATE,
    deletedBy: DataTypes.STRING
  },
  {
    freezeTableName: true,
    paranoid: true
  },
  {
    indexes: [{
      fields: ['email', 'username', 'firstName', 'lastName'],
      primary: false,
      unique: false
    }],
    defaultScope: {
      attributes: { exclude: ['passwordHash', 'salt', 'previousPasswords'] }
    }
  });
  Operator.associate = function({ OperatorRole }) {
    Operator.hasOne(OperatorRole, {foreignKey: 'operatorId'});
  };
  return Operator;
};

'use strict';

module.exports = function (sequelize, DataTypes) {
  const DeviceLog = sequelize.define('DeviceLog', {
    deviceId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    timestamp: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    info: DataTypes.JSONB
  },
  {
      freezeTableName: true,
      timestamps: false
  });

  return DeviceLog;
};

'use strict';

module.exports = function (sequelize, DataTypes) {
  const Report = sequelize.define('Report', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    timestamp: DataTypes.INTEGER,
    deviceId: DataTypes.INTEGER,
    type: DataTypes.STRING,
    status: DataTypes.STRING,
    priority: DataTypes.INTEGER,
    info: DataTypes.JSONB,
    notes: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    createdBy: DataTypes.INTEGER,
    updatedAt: DataTypes.DATE,
    updatedBy: DataTypes.INTEGER,
    deletedAt: DataTypes.DATE,
    deletedBy: DataTypes.INTEGER
  },
  {
    freezeTableName: true,
    paranoid: true
  },
  {
    indexes: [{
      fields: ['timestamp', 'deviceId', 'status', 'type', 'priority'],
      primary: false,
      unique: false
    }]
  });
  Report.associate = function({ Device }) {
    Report.belongsTo(Device, {foreignKey: 'deviceId'});
  };
  return Report;
};

'use strict';

module.exports = function (sequelize, DataTypes) {
  const SensorData = sequelize.define('SensorData', {
    deviceId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    timestamp: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    windSpeed: DataTypes.DOUBLE,
    windDirection: DataTypes.DOUBLE,
    temp: DataTypes.DOUBLE
  },
  {
    freezeTableName: true,
    timestamps: false
  },
  {
    indexes: [{
      fields: ['deviceId'],
      primary: false,
      unique: false
    }]
  });

  return SensorData;
};

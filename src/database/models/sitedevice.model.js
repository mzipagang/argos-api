'use strict';

module.exports = function (sequelize, DataTypes) {
  const SiteDevice = sequelize.define('SiteDevice', {
    siteId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    deviceId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    }
  },
  {
    freezeTableName: true,
    timestamps: false
  },
  {
    indexes: [{
      fields: ['siteId', 'deviceId'],
      primary: false,
      unique: false
    }]
  });
  SiteDevice.associate = function({ Device }) {
    SiteDevice.hasOne(Device, {sourceKey: 'deviceId', foreignKey: 'id'});
  };
  return SiteDevice;
};

'use strict';

module.exports = function (sequelize, DataTypes) {
    const Site = sequelize.define('Site', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        parentSite: DataTypes.INTEGER,
        name: DataTypes.STRING,
        type: DataTypes.STRING,
        lat: DataTypes.DOUBLE,
        lng: DataTypes.DOUBLE,
        notes: DataTypes.STRING,
        createdAt: DataTypes.DATE,
        createdBy: DataTypes.STRING,
        updatedAt: DataTypes.DATE,
        updatedBy: DataTypes.STRING,
        deletedAt: DataTypes.DATE,
        deletedBy: DataTypes.STRING
    },
    {
        freezeTableName: true,
        paranoid: true
    });
    return Site;
};

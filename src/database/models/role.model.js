'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: DataTypes.STRING,
    parent: DataTypes.INTEGER,
    permissions: DataTypes.JSONB
  },
  {
    freezeTableName: true,
    timestamps: false
  }, {});
  Role.associate = function({ OperatorRole }) {
    
  };
  return Role;
};

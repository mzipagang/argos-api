'use strict';

module.exports = function (sequelize, DataTypes) {
  const OperatorRole = sequelize.define('OperatorRole', {
    operatorId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    roleId: {
      type: DataTypes.INTEGER,
      primaryKey: true
    }
  },
  {
    freezeTableName: true,
    timestamps: false
  },
  {
    indexes: [{
      fields: ['operatorId', 'roleId'],
      primary: false,
      unique: false
    }]
  });
  OperatorRole.associate = function({ Role }) {
    OperatorRole.hasOne(Role, {sourceKey: 'roleId', foreignKey: 'id'});
  };
  return OperatorRole;
};

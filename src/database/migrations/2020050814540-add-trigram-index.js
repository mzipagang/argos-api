const vectorName = '_search';

const searchObjects = {
    'Site': [
        'name',
        'type',
        'notes'
    ],
    'Device': [
        'serial'
    ],
    'Operator': [
        'email',
        'firstName',
        'lastName',
        'username'
    ],
    'Report': [
        'type',
        'status'
    ],
    'Role': [
        'name'
    ]
};

module.exports = {
    up: queryInterface => {
        return queryInterface.sequelize.transaction((t) => {
                return Promise.all(Object.keys(searchObjects).map(table => {
                        return queryInterface.sequelize.query(`CREATE INDEX "${table}_trigram_idx" ON "${table}" USING gin (${searchObjects[table].map(c => `"${c}" gin_trgm_ops`).join(', ')});`, {transaction: t})
                            .error(console.log);
                    }
                ));
            }
        );
    },

    down: queryInterface => (
        queryInterface.sequelize.transaction((t) =>
            Promise.all(Object.keys(searchObjects).map(table => {
                    return queryInterface.sequelize.query(`DROP INDEX "${table}_trigram_idx";`, {transaction: t});
                }
            ))
        )
    ),
};

'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Device', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      serial: {
        type: Sequelize.STRING,
        unique: 'session_unique'
      },
      lat: {
        type: Sequelize.DOUBLE
      },
      lng: {
        type: Sequelize.DOUBLE
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        type: Sequelize.DATE
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedAt: {
        type: Sequelize.DATE
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      deletedAt: {
        type: Sequelize.DATE
      },
      deletedBy: {
        type: Sequelize.STRING
      }
    }, {
      uniqueKeys: {
        session_unique: {
          fields: ['serial']
        }
      }
    })
    .then(() => {
        return queryInterface.addIndex('Device', ['serial']);
    });
  },
  down: (queryInterface, _Sequelize) => {
    return queryInterface.dropTable('Device');
  }
};
'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SiteDevice', {
      siteId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      deviceId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SiteDevice');
  }
};
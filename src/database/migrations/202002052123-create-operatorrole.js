'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('OperatorRole', {
      operatorId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      roleId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('OperatorRole');
  }
};
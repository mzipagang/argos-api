'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DeviceLog', {
      deviceId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      timestamp: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      info: {
        type: Sequelize.JSONB,
        allowNull: false
      }
    });
  },
  down: (queryInterface, _Sequelize) => {
    return queryInterface.dropTable('DeviceLog');
  }
};
'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SensorData', {
      deviceId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      timestamp: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      windSpeed: {
        type: Sequelize.DOUBLE
      },
      windDirection: {
        type: Sequelize.DOUBLE
      },
      temp: {
        type: Sequelize.DOUBLE
      }
    });
  },
  down: (queryInterface, _Sequelize) => {
    return queryInterface.dropTable('SensorData');
  }
};
'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Photo', {
      deviceId: {
        type: Sequelize.INTEGER
      },
      timestamp: {
        type: Sequelize.INTEGER
      },
      result: {
        type: Sequelize.JSONB
      }
    });
  },
  down: (queryInterface, _Sequelize) => {
    return queryInterface.dropTable('Photo');
  }
};

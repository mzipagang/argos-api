exports.events = {
  getPhotos: 'get.photos',
  getSensorData: 'get.sensor.data'
};
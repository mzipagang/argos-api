'use strict';

const path = require('path');
const _ = require('lodash');

// All configurations will extend these options
// ============================================
process.env.NODE_ENV =  process.env.NODE_ENV || 'development'
const all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(__dirname + '/../../..'),

  // Server port
  port: process.env.PORT,

  super_secret: process.env.SUPER_SECRET || 'hippopotamus',

  origins: process.env.ORIGINS || 'http://localhost:3000',

  smtp: {
    sender: process.env.SMTP_SENDER || 'mzipagang@boldtype.com',
    password: process.env.SMTP_PASSWORD || 's3s#ma!l1',
    host: process.env.SMTP_HOST || 'mail.smtp2go.com',
    port: 587,
    secure: false
  },

  sts: {
    key: process.env.AWS_ACCESS_KEY,
    secret: process.env.AWS_SECRET_KEY,
    region: 'us-west-2'
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js'));

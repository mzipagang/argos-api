const getPaddedTimes = (center, seconds) => {
    const totalMilliseconds = seconds * 1000;
    const edgeVal = totalMilliseconds / 2;
    center = parseInt(center);

    return {
        start: center - edgeVal >= 0 ? Math.abs(Math.round(center - edgeVal)) : 0,
        end: Math.abs(Math.round(center + edgeVal)),
    };
};

module.exports = {
    getPaddedTimes
};
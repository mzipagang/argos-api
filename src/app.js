const config = require('./config');
const routes = require('./routes');
const socketRoutes = require('./socketRoutes');
const swagger = require('./config/swagger');
const socketIo = require('socket.io');

// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: {
        level: 'error'
    }
});

fastify.register(require('fastify-cors'), {
    origin: config.origins.includes('_') ? config.origins.split('_') : config.origins.split(','),
    methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
});

// ping endpoint
fastify.get('/', async (request, reply) => {
    return {status: 200}
});

fastify.register(require('fastify-swagger'), swagger.options);

fastify.register(require('fastify-jwt'), {
  secret: config.super_secret,
  cookie: {
    cookieName: 'token'
  }
});

fastify.register(require('fastify-cookie'), {
  secret: config.super_secret
})

fastify.addHook("onRequest", async (request, reply) => {
    try {
      if(request.raw.url.includes('operator/login')
        || request.raw.url.includes('documentation')
        || request.raw.url.includes('resetPassword')
        || request.raw.url.includes('requestResetPassword')){
        return;
      }
      await request.jwtVerify();
    } catch (err) {
      reply.send(err)
    }
});

const io = socketIo(fastify.server);

// route setup
routes(fastify);
socketRoutes(io, fastify);

// require('./cron');

const start = async () => {
    try {
        await fastify.listen(config.port, '0.0.0.0');
        fastify.swagger();
        console.log(`listening on ${fastify.server.address().port}`)
    } catch (err) {
        console.log(err);
        process.exit(1)
    }
};

start();

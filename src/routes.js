/**
 * Main application routes
 */

'use strict';

module.exports = fastify => {
  [
    './api/v1/operator',
    // './api/v1/report',
    './api/v1/site',
    './api/v1/device',
    './api/v1/sensordata',
    './api/v1/devicelog',
    './api/v1/photo'
  ].forEach(path => {
  	require(path).forEach(route => {
      fastify.route(route);
    })
  })
}

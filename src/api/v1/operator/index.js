const controller = require('./operator.controller');

const routes = [
    {
        method: 'POST',
        url: '/api/v1/operator/search',
        handler: controller.search,
        schema: {
            description: 'Takes in search criteria and returns search results',
            body: {
                type: 'object',
                properties: {
                    text: {
                        type: 'string',
                        description: 'search string'
                    },
                    from: {
                        type: 'integer',
                        description: 'start index of results to return'
                    },
                    limit: {
                        type: 'integer',
                        description: 'max number of results to return'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                type: 'string'
                            },
                            email: {
                                type: 'string'
                            },
                            firstName: {
                                type: 'string'
                            },
                            lastName: {
                                type: 'string'
                            },
                            title: {
                                type: 'string'
                            },
                            role: {
                                type: 'string'
                            },
                            createdAt: {
                                type: 'string'
                            },
                            updatedAt: {
                                type: 'string'
                            },
                            deletedAt: {
                                type: 'string'
                            }
                        }
                    }
                }
            }
        }
    },
    {
        method: 'POST',
        url: '/api/v1/operator',
        handler: controller.create,
        schema: {
            description: 'Takes in user data and creates an operator',
            body: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    },
                    username: {
                        type: 'string'
                    },
                    password: {
                        type: 'string'
                    },
                    firstName: {
                        type: 'string'
                    },
                    lastName: {
                        type: 'string'
                    },
                    roleId: {
                        type: 'string'
                    },
                    baseUrl: {
                        type: 'string',
                        baseUrl: 'used as the base url for the email link'
                    }
                }
            },
            response: {
                201: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'PUT',
        url: '/api/v1/operator/:email',
        handler: controller.update,
        schema: {
            description: 'Takes in an email and user data and updates an operator',
            params: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string',
                        description: 'operator email'
                    }
                }
            },
            body: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    },
                    username: {
                        type: 'string'
                    },
                    firstName: {
                        type: 'string'
                    },
                    lastName: {
                        type: 'string'
                    },
                    roleId: {
                        type: 'string'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'PATCH',
        url: '/api/v1/operator/:email/requestResetPassword',
        handler: controller.requestResetPassword,
        schema: {
            description: 'Request an operator password reset email',
            params: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    }
                }
            },
            body: {
                type: 'object',
                properties: {
                    baseUrl: {
                        type: 'string',
                        baseUrl: 'used as the base url for the email link'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'PATCH',
        url: '/api/v1/operator/:email/resetPassword',
        handler: controller.resetPassword,
        schema: {
            description: 'Takes in an email, activateToken and password and resets the password',
            params: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string',
                        description: 'operator email'
                    }
                }
            },
            body: {
                type: 'object',
                properties: {
                    activateToken: {
                        type: 'string'
                    },
                    password: {
                        type: 'string'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object',
                    properties: {
                        id: {
                            type: 'string'
                        },
                        email: {
                            type: 'string'
                        },
                        role: {
                            type: 'string'
                        },
                        permissions: {
                            type: 'string'
                        }
                    }
                }
            }
        }
    },
    {
        method: 'POST',
        url: '/api/v1/operator/login',
        handler: controller.login,
        schema: {
            description: 'Takes in an email and password and returns operator data',
            body: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    },
                    password: {
                        type: 'string'
                    },
                    rememberMe: {
                        type: 'boolean'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object',
                    properties: {
                        id: {
                            type: 'string'
                        },
                        email: {
                            type: 'string'
                        },
                        firstName: {
                            type: 'string'
                        },
                        lastName: {
                            type: 'string'
                        },
                        username: {
                            type: 'string'
                        },
                        role: {
                            type: 'string'
                        },
                        permissions: {
                            type: 'string'
                        }
                    }
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/operator/:email',
        handler: controller.get,
        schema: {
            description: 'Gets an operator by email',
            params: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object',
                    properties: {
                        id: {
                            type: 'string'
                        },
                        email: {
                            type: 'string'
                        },
                        username: {
                            type: 'string'
                        },
                        firstName: {
                            type: 'string'
                        },
                        lastName: {
                            type: 'string'
                        },
                        title: {
                            type: 'string'
                        },
                        role: {
                            type: 'object',
                            properties: {
                                name: {
                                    type: 'string'
                                },
                                id: {
                                    type: 'string'
                                },
                                permissions: {
                                    type: 'object'
                                }
                            }
                        },
                        createdAt: {
                            type: 'string'
                        },
                        createdBy: {
                            type: 'string'
                        },
                        updatedAt: {
                            type: 'string'
                        },
                        updatedBy: {
                            type: 'string'
                        },
                        deletedAt: {
                            type: 'string'
                        },
                        deletedBy: {
                            type: 'string'
                        }
                    }
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/operator/:from/:limit',
        handler: controller.all,
        schema: {
            description: 'Gets operators',
            params: {
                type: 'object',
                properties: {
                    from: {
                        type: 'integer',
                        description: 'start index of results to return'
                    },
                    limit: {
                        type: 'integer',
                        description: 'max number of results to return'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                type: 'string'
                            },
                            email: {
                                type: 'string'
                            },
                            username: {
                                type: 'string'
                            },
                            firstName: {
                                type: 'string'
                            },
                            lastName: {
                                type: 'string'
                            },
                            title: {
                                type: 'string'
                            },
                            role: {
                                type: 'object',
                                properties: {
                                    name: {
                                        type: 'string'
                                    },
                                    id: {
                                        type: 'string'
                                    },
                                    permissions: {
                                        type: 'object'
                                    }
                                }
                            },
                            permissions: {
                                type: 'object'
                            },
                            createdAt: {
                                type: 'string'
                            },
                            createdBy: {
                                type: 'string'
                            },
                            updatedAt: {
                                type: 'string'
                            },
                            updatedBy: {
                                type: 'string'
                            },
                            deletedAt: {
                                type: 'string'
                            },
                            deletedBy: {
                                type: 'string'
                            }
                        }
                    }
                }
            }
        }
    },
    {
        method: 'DELETE',
        url: '/api/v1/operator/:email',
        handler: controller.destroy,
        schema: {
            description: 'Deletes an operator by email',
            params: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    }
                }
            },
            response: {
                204: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/operator/:email/undelete',
        handler: controller.undelete,
        schema: {
            description: 'Undeletes an operator by email',
            params: {
                type: 'object',
                properties: {
                    email: {
                        type: 'string'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    }
];

module.exports = routes;

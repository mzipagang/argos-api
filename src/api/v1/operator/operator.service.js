const models = require('../../../database/models');
const { QueryTypes } = require('sequelize');
const {Operator, OperatorRole, Role, Sequelize, sequelize} = models;
const {Op} = Sequelize;
const {encryptPassword, isAuthenticated} = require('../auth/auth.service');

const get = async email => {
    if (!email) {
        return null;
    }
    const data = await Operator.findOne({
        where: {
            email
        },
        include: [
            {
                model: OperatorRole,
                include: [
                    Role
                ]
            }
        ],
        paranoid: false
    });
    if (data && data.dataValues) {
        const { id, email, username, firstName, lastName, title, createdAt, createdBy, updatedAt, updatedBy, deletedAt, deletedBy } = data.dataValues;
        const role = data.dataValues.OperatorRole && data.dataValues.OperatorRole.dataValues && data.dataValues.OperatorRole.dataValues.Role &&
            data.dataValues.OperatorRole.dataValues.Role.dataValues ? data.dataValues.OperatorRole.dataValues.Role.dataValues : {};
        return { id, email, username, firstName, lastName, title, role: { name: role.name, id: role.id, permissions: role.permissions || [] }, createdAt, createdBy, updatedAt, updatedBy, deletedAt, deletedBy };
    } else {
        return null;
    }
};

const getActivateToken = async email => {
    if (!email) {
        return null;
    }
    const data = await Operator.findOne({
        attributes: [
            'passwordHash',
        ],
        where: { email },
        paranoid: false
    });
    if (data && data.dataValues) {
        const { passwordHash: activateToken } = data.dataValues;
        return activateToken;
    } else {
        return null;
    }
};

const all = async params => {
    const {
        from = 0,
        limit = 30
    } = params;
    const data = await Operator.findAll({
        include: [
            {
                model: OperatorRole,
                include: [
                    Role
                ]
            }
        ],
        order: [
            ['email', 'ASC']
        ],
        offset: from,
        limit,
        paranoid: false
    });
    const results = data.map(d => {
        const { id, email, username, firstName, lastName, title, createdAt, createdBy, updatedAt, updatedBy, deletedAt, deletedBy } = d.dataValues;
        const role = d.dataValues.OperatorRole && d.dataValues.OperatorRole.dataValues && d.dataValues.OperatorRole.dataValues.Role &&
            d.dataValues.OperatorRole.dataValues.Role.dataValues ? d.dataValues.OperatorRole.dataValues.Role.dataValues : {};
        return { id, email, username, firstName, lastName, title, role: { name: role.name, id: role.id, permissions: role.permissions || [] }, createdAt, createdBy, updatedAt, updatedBy, deletedAt, deletedBy };
    });
    return results;
};

const login = async params => {
    const {email, password} = params;
    if (!email || !password) {
        return { id: null };
    }
    const data = await Operator.findOne({
        where: {
            email
        },
        include: [
            {
                model: OperatorRole,
                include: [
                    Role
                ]
            }
        ]
    });
    if (data && data.dataValues && isAuthenticated(password, data.dataValues.salt, data.dataValues.passwordHash)) {
        const { id, email, username, firstName, lastName, title } = data.dataValues;
        const role = data.dataValues.OperatorRole && data.dataValues.OperatorRole.dataValues && data.dataValues.OperatorRole.dataValues.Role &&
            data.dataValues.OperatorRole.dataValues.Role.dataValues ? data.dataValues.OperatorRole.dataValues.Role.dataValues : {};
        return { id, email, username, firstName, lastName, title, role: role.name, permissions: role.permissions || [] };
    } else {
        return { id: null };
    }
};

const create = async (params, by) => {
    if (!isValidEmail(params.email)) {
        return { code: 409, err: 'Invalid email' };
    }
    if (params.password && !isValidPassword(params.password)) {
        return { code: 409, err: 'Invalid password' };
    }
    const operator = await Operator.findOne({
        where: {
            email: params.email
        },
        paranoid: false
    });
    if (!(params.roleId) || params.roleId === '' || isNaN(params.roleId)) {
        return { code: 409, err: 'Missing required parameters' };
    } else if (!operator) {
        const password =  params.password || generatePassword(8);
        const { hash, salt } = encryptPassword(password);
        params.passwordHash = hash;
        params.salt = salt;
        params.createdBy = by;
        params.updatedBy = by;
        try {
            const result = await sequelize.transaction(async t => {
                const result = await Operator.create(params, { transaction: t });
                await OperatorRole.create({
                    operatorId: result.id,
                    roleId: params.roleId
                }, { transaction: t });
                return { code: 200, activateToken: hash };
            });
            return result;
        } catch (err) {
            return { code: 409, err };
        }
    } else {
        return { code: 409, err: 'Email already taken' };
    }
};

const resetPassword = async (email, params) => {
    const { activateToken, password } = params;
    if (!isValidPassword(password)) {
        return { code: 409, err: 'Invalid password' };
    }
    if (!email) {
        return { code: 409, err: 'Missing email' };
    }
    if(!activateToken){
        return { code: 409, err: 'Missing token' };
    }
    const data = await Operator.findOne({
        where: {
            email,
            passwordHash: activateToken
        },
        paranoid: false
    });
    if (data && data.dataValues && activateToken === data.dataValues.passwordHash) {
        let { previousPasswords = []}  = data.dataValues;
        if (previousPasswords) {
            for (let i = 0; i < previousPasswords.length; i++) {
                const { salt, passwordHash } = previousPasswords[i];
                const { hash } = encryptPassword(password, salt);
                if (passwordHash === hash) {
                    return { code: 409, err: 'Choose a password that is different from your last 5 passwords' };
                }
            }
        }
        const { hash: passwordHash, salt } = encryptPassword(password);
        previousPasswords = previousPasswords || [];
        previousPasswords.unshift({
            passwordHash,
            salt
        });
        while (previousPasswords.length > 5) {
            previousPasswords.pop();
        }
        await Operator.update({ passwordHash, salt, previousPasswords }, {
            where: { email },
            paranoid: false
        });
        return { code: 200 };
    } else {
        return { code: 409, err: 'Incorrect email or token' };
    }
}

const update = async (email, params, by) => {
    if (!isValidEmail(email)) {
        return { code: 409, err: 'Invalid email' };
    }
    const operator = await Operator.findOne({
        where: { email },
        paranoid: false
    });
    if (operator) {
        const otherOperator = await Operator.findOne({
            where: {
                email: params.email,
                id: {
                    [Op.ne]: operator.id
                }
            },
            paranoid: false
        });
        if (!otherOperator) {
            delete params.passwordHash;
            delete params.salt;
            delete params.previousPasswords;
            params.updatedBy = by;
            try {
                const result = await sequelize.transaction(async t => {
                    await Operator.update(params, {
                        where: { email },
                        paranoid: false
                    }, { transaction: t });
                    await OperatorRole.update({
                        operatorId: operator.id,
                        roleId: params.roleId
                    }, {
                        where: { operatorId: operator.id }
                    }, { transaction: t });
                    return { code: 200 };
                });
                return result;
            } catch (err) {
                return { code: 409, err };
            }
        } else {
            return { code: 409, err: 'Email already taken' };
        }
    } else {
        return { code: 404, err: 'User not found' };
    }
};

const destroy = async (email, by) => {
    try {
        await sequelize.transaction(async t => {
            await Operator.update({
                deletedBy: by
            }, {
                where: { email }
            }, { transaction: t });
            await Operator.destroy({
                where: { email }
            }, { transaction: t });
        });
    } catch (err) {
        throw err;
    }
};

const undelete = async (email, by) => {
    await Operator.update({
        deletedAt: null,
        deletedBy: null,
        updatedBy: by
    }, {
        where: { email },
        paranoid: false
    });
};

const search = async params => {
    const {
        text,
        from = 0,
        limit = 30,
    } = params;
    let where = '';
    const replacements = { from, limit };
    if (text) {
        where = `where cast(o.id as varchar) ilike :text
        or concat(o."firstName", ' ', o."lastName" ) ilike :text
        or o.title ilike :text
        or o.email ilike :text
        or r."name" ilike :text`;
        replacements.text = `%${text}%`;
    }
    const operators = await sequelize.query(
        `select distinct o.id, o."firstName", o."lastName", o.title, o.email, o."createdAt", o."deletedAt", o."updatedAt",
            r."name" as "role"
        from "${Operator.tableName}" o
        left join "${OperatorRole.tableName}" opr on o.id = opr."operatorId"
        left join "${Role.tableName}" r on opr."roleId" = r.id
        ${where} limit :limit offset :from`,
        {
            replacements,
            type: QueryTypes.SELECT
        }
    );
    return operators;
};

const generatePassword = (length = 8) => {
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
};

const isValidEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email && re.test(email);
};

const isValidPassword = (password) => {
    const re = /^(?=.*[A-Z])(?=.*[!"#$%&'()*+,-./:;=?@[\]^_`{|}~])(?=.*[a-z])(?=.*[0-9])[a-zA-Z0-9!"#$%&'()*+,-./:;=?@[\]^_`{|}~]{8,100}$/;
    return password && re.test(password);
};

module.exports = {
    get,
    getActivateToken,
    login,
    create,
    update,
    all,
    destroy,
    undelete,
    resetPassword,
    search
};

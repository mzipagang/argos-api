const boom = require('boom');
const { create, update, login, get, getActivateToken, resetPassword, all, destroy, undelete, search } = require('./operator.service');
const { sendActivationEmail, sendResetPasswordEmail } = require('../email/email.service');

exports.create = async (req, reply) => {
    if (req.user && req.user.role && req.user.role.includes('Admin')) {
        const { code, err, activateToken } = await create(req.body, req.user.email);
        if (code === 200) {
            if (req.body.baseUrl) {
                await sendActivationEmail({ email: req.body.email, activateToken, baseUrl: req.body.baseUrl });
            }
            reply.code(code).send({ activateToken });
        } else {
            reply.code(code).send(err);
        }
    } else {
        reply.code(409).send('Unauthorized request');
    }
};

exports.update = async (req, reply) => {
    if (req.user && req.user.role && req.user.role.includes('Admin')) {
        const { code, err } = await update(req.params.email, req.body, req.user.email);
        if (code === 200) {
            reply.code(code).send();
        } else {
            reply.code(code).send(err);
        }
    } else {
        reply.code(409).send('Unauthorized request');
    }
};

exports.requestResetPassword = async (req, reply) => {
    const activateToken = await getActivateToken(req.params.email ? req.params.email.trim() : '');
    if (activateToken) {
        if (req.body.baseUrl) {
            await sendResetPasswordEmail({ email: req.params.email, activateToken, baseUrl: req.body.baseUrl });
            reply.code(200).send();
        } else {
            reply.code(409).send('Missing base url');
        }
    } else {
        reply.code(409).send('Invalid request');
    }
};

exports.resetPassword = async (req, reply) => {
    const { code, err } = await resetPassword(req.params.email, req.body);
    if (code === 200) {
        const { id, email, role, permissions } = await login({ email: req.params.email, password: req.body.password });
        if (id) {
            const token = await reply.jwtSign({ id, email, role, permissions });
            reply
                .setCookie('token', token, {
                    // secure: true,
                    httpOnly: true,
                    sameSite: true,
                    path: '/',
                })
                .code(200)
                .send({ id, email, role, permissions });
        } else {
            reply.code(409).send('Unauthorized request');
        }
    } else {
        reply.code(code).send(err);
    }
};

exports.login = async (req, reply) => {
    const { id, email, firstName, lastName, username, role, permissions } = await login(req.body);
    if (id) {
        const token = await reply.jwtSign({ id, email, firstName, lastName, username, role, permissions });
        const options = {
            // secure: true,
            httpOnly: true,
            sameSite: true,
            path: '/'
        };
        if (req.body.rememberMe) {
            options.maxAge = 604800;
        }
        reply
            .setCookie('token', token, options)
            .code(200)
            .send({ id, email, firstName, lastName, username, role, permissions });
    } else {
        reply.code(401).send('Incorrect login');
    }
};

exports.get = async (req, reply) => {
    const operator = await get(req.params.email);
    if (operator) {
        reply.code(200).send(operator);
    } else {
        reply.code(404).send();
    }
};

exports.destroy = async (req, reply) => {
    await destroy(req.params.email, req.user.email);
    reply.code(204).send();
};

exports.undelete = async (req, reply) => {
    await undelete(req.params.email, req.user.email);
    reply.code(200).send();
};

exports.all = async (req, reply) => {
    const operators = await all(req.params);
    reply.code(200).send(operators);
};

exports.search = async (req, reply) => {
    const results = await search(req.body);
    reply.code(200).send(results);
};
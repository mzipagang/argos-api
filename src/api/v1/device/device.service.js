const models = require('../../../database/models');
const { QueryTypes } = require('sequelize');
const {Device, SiteDevice, Sequelize, sequelize} = models;
const {Op} = Sequelize;

const get = async id => {
    if (!id) {
        return null;
    }
    const data = await Device.findOne({
        where: { id },
        paranoid: false
    });
    if (data && data.dataValues) {
        return data.dataValues;
    } else {
        return null;
    }
};

const getBySite = async siteId => {
    if (!siteId) {
        return null;
    }
    const data = await Device.findAll({
        include: [
            {
                model: SiteDevice,
                where: { siteId }
            }
        ],
        paranoid: false
    });
    return data;
};

const all = async params => {
    const {
        from = 0,
        limit = 30
    } = params;
    const data = await Device.findAll({
        order: [
            ['createdAt', 'ASC']
        ],
        offset: from,
        limit,
        paranoid: false
    });
    return data;
};

const create = async (params, by) => {
    delete params.deletedBy;
    delete params.deletedAt;
    params.createdBy = by;
    params.updatedBy = by;
    await Device.create(params);
};

const update = async (id, params, by) => {
    delete params.id;
    delete params.deletedBy;
    delete params.deletedAt;
    params.updatedBy = by;
    const rowsUpdated = await Device.update(params, {
        where: { id },
        paranoid: false
    });
    return rowsUpdated;
};

const destroy = async (id, by) => {
    try {
        await sequelize.transaction(async t => {
            await Device.update({
                deletedBy: by
            }, {
                where: { id }
            }, { transaction: t });
            await Device.destroy({
                where: { id }
            }, { transaction: t });
        });
    } catch (err) {
        throw err;
    }
};

const search = async params => {
    const {
        text,
        from = 0,
        limit = 30
    } = params;
    let where = '';
    const replacements = { from, limit };
    if (text) {
        where = `where (cast(s.id as varchar) ilike :text
        or s.serial ilike :text)`;
        replacements.text = `%${text}%`;
    }
    const devices = await sequelize.query(
        `select s.id, s.serial, s.lat, s.lng, s.status
        from "${Device.tableName}" s
        ${where} limit :limit offset :from`,
        {
            replacements,
            type: QueryTypes.SELECT
        }
    );
    return devices;
};

module.exports = {
    get,
    getBySite,
    create,
    update,
    all,
    destroy,
    search
};

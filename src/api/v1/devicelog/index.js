const controller = require('./devicelog.controller');

const routes = [
    {
        method: 'POST',
        url: '/api/v1/devicelog',
        handler: controller.create,
        schema: {
            description: 'Takes in data and creates a device log',
            body: {
                type: 'object',
                properties: {
                    deviceId: {
                        type: 'integer'
                    },
                    timestamp: {
                        type: 'string'
                    },
                    info: {
                        type: 'string'
                    }
                }
            },
            response: {
                201: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/devicelog/:deviceId/:from/:limit',
        handler: controller.all,
        schema: {
            description: 'Gets device logs',
            params: {
                type: 'object',
                properties: {
                    deviceId: {
                        type: 'integer'
                    },
                    from: {
                        type: 'integer',
                        description: 'start index of results to return'
                    },
                    limit: {
                        type: 'integer',
                        description: 'max number of results to return'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            deviceId: {
                                type: 'string'
                            },
                            timestamp: {
                                type: 'string'
                            },
                            info: {
                                type: 'string'
                            }
                        }
                    }
                }
            }
        }
    }
];

module.exports = routes;

const models = require('../../../database/models');
const { QueryTypes } = require('sequelize');
const {DeviceLog, Sequelize, sequelize} = models;
const {Op} = Sequelize;

const all = async params => {
    const {
        deviceId,
        from = 0,
        limit = 30
    } = params;
    const data = await DeviceLog.findAll({
        where: { deviceId },
        order: [
            ['timestamp', 'DESC']
        ],
        offset: from,
        limit
    });
    return data;
};

const create = async params => {
    await DeviceLog.create(params);
};

module.exports = {
    create,
    all
};

const boom = require('boom');
const { create, all } = require('./devicelog.service');

exports.create = async (req, reply) => {
    await create(req.body, req.user.email);
    reply.code(201).send();
};

exports.all = async (req, reply) => {
    const data = await all(req.params);
    reply.code(200).send(data);
};

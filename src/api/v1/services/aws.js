'use strict';

const AWS = require('aws-sdk');
const crypto = require('crypto');
const config = require('../../../config');

const sts = new AWS.STS({accessKeyId: config.sts.key, secretAccessKey: config.sts.secret, region: config.sts.region});
const s3 = new AWS.S3({accessKeyId: config.sts.key, secretAccessKey: config.sts.secret, region: config.sts.region});

const bucket = 'argos-sensor-images';

exports.getS3Object = filename => new Promise((resolve, reject) => {
    const getParams = {
        Bucket: bucket,
        Key: `${filename}`
    }
    s3.getObject(getParams, function(err, data) {
        if (err) {
            console.log('getObject failed: ', filename, err);
            return reject(err);
        }
        resolve(data.Body.toString('utf-8'));
    });
});

exports.getS3UploadAccess = filename => {
    const params = {
        DurationSeconds: 900
    };
    return new Promise((resolve, reject) => {
        sts.getSessionToken(params, (err, data) => {
            if (err) {
                return reject(err);
            }
            if (!data) {
                return resolve(null);
            }
            const policy = {
                "expiration": new Date(new Date().getTime() + 1000 * 60 * 5).toISOString(),
                "conditions": [
                    {"bucket": bucket},
                    {"key": filename},
                    {"acl": 'public-read'},
                    {"x-amz-security-token": data.Credentials.SessionToken},
                    ["starts-with", "$Content-Type", ""],
                    ["content-length-range", 0, 5242880]
                ]
            };
            const policyBase64 = new Buffer(JSON.stringify(policy), 'utf8').toString('base64');
            const signature = crypto.createHmac('sha1', data.Credentials.SecretAccessKey).update(policyBase64).digest('base64');
            resolve({
                bucket: bucket,
                awsKey: data.Credentials.AccessKeyId,
                secretKey: data.Credentials.SecretAccessKey,
                policy: policyBase64,
                signature: signature,
                sessionToken: data.Credentials.SessionToken
            });
        });
    });
};

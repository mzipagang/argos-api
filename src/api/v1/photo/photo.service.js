const models = require('../../../database/models');
const {Photo, Sequelize, sequelize} = models;
const { QueryTypes } = require('sequelize');
const {Op} = Sequelize;
const { getPaddedTimes } = require('../../../helpers/helpers');
const { getS3UploadAccess, getS3Object } = require('../services/aws');

const getPhotosByCenter = async (deviceId, centerTimestamp, seconds) => {
  const query = {
      where: {
        deviceId
      },
      order: [
          ['timestamp', 'ASC']
      ]
  };

  const paddedTimes = getPaddedTimes(centerTimestamp, seconds);

  query.where.timestamp = {
      [Op.lt]: paddedTimes.end,
      [Op.gte]: paddedTimes.start,
  };

  const data = await Photo.findAll(query);
  return data;
};

const create = async params => {
    delete params.result;
    await Photo.create(params);
};

const getUploadAccess = async params => {
    return await getS3UploadAccess(params.filename);
}

const getObject = async params => {
  return await getS3Object(params.filename);
}

const unread = async params => {
  const {
      timestamp
  } = params;
  const replacements = { timestamp };
  const data = await sequelize.query(
      `select p.*,
      (select sd."siteId" from "SiteDevice" sd where p."deviceId" = sd."deviceId") as "siteId"
      from "Photo" p
      where "timestamp" >= :timestamp and result is null
      order by "timestamp" asc`,
      {
          replacements,
          type: QueryTypes.SELECT
      }
  );
  return data;
};

const update = async params => {
  await Photo.update(params, {
    where: {
      deviceId: params.deviceId,
      timestamp: params.timestamp
    }
  });
};

const buildPhotosProtoBuffer = (proto, photos) => {
  return new Promise(resolve => {
    const PhotoMessages = proto.lookupType("photopackage.PhotoMessages");
    const payload = {
      photos
    };
    const errMsg = PhotoMessages.verify(payload);
    if (errMsg) {
      console.log(errMsg);
      throw Error(errMsg);
    }
    const message = PhotoMessages.create(payload);
    return resolve(PhotoMessages.encode(message).finish());
  });
};

module.exports = {
    create,
    unread,
    update,
    getPhotosByCenter,
    buildPhotosProtoBuffer,
    getUploadAccess,
    getObject
};

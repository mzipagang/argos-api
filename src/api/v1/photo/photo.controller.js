const boom = require('boom');
const { create, getPhotosByCenter, getUploadAccess, buildPhotosProtoBuffer, unread, update, getObject } = require('./photo.service');
const { loadPhotoProto } = require('../../../../protobuf-models');
const socketConfig = require('../../../config/socket');
const cron = require('node-cron');

const fireTags = {
    'fire': 1,
    'smoke': 1,
    'volcano': 1,
    'eruption': 1
};

const sockets = {};

cron.schedule('*/3 * * * * *', async () => {
  const data = await unread({ timestamp: Math.floor((Date.now() - 300000) / 1000) });
  console.log(`${data.length} images to process`);
  if (data && data.length) {
    await Promise.all(
      data.map(d => new Promise(async (resolve) => {
        const photoData = d;
        try {
          const resultJson = await getObject({ filename: `${photoData.deviceId}-${photoData.timestamp}.json` });
          const result = JSON.parse(resultJson);
          if (result && result.Labels && result.Labels.length) {
            let alertTag = '';
            let alertConfidence = 0;
            result.Labels.some(l => {
              if (fireTags[(l.Name || '').toLowerCase()]) {
                alertTag = l.Name;
                alertConfidence = l.Confidence;
                photoData.result = JSON.stringify({ alertTag, alertConfidence });
                return true;
              }
              return false;
            });
            photoData.result = photoData.result || JSON.stringify({});
            await update(photoData);
          }
          resolve();
        } catch (e) {
          console.log(`cannot get ${photoData.deviceId}-${photoData.timestamp}.json`);
          resolve()
        }
      }))
    );
    Object.keys(sockets).forEach(id => {
      const socket = sockets[id];
      return Promise.all([
        loadPhotoProto(),
        Promise.resolve(data.filter(d => d.result))
      ])
      .then(response => {
        const protoModel = response[0];
        const photos = response[1];
        return buildPhotosProtoBuffer(protoModel, photos);
      })
      .then(responseBuffer => {
        return socket.emit(socketConfig.events.getPhotos, responseBuffer);
      })
      .catch(err => {
        console.log(err);
        return socket.emit(socketConfig.events.getPhotos, err.message ? err.message : err);
      });
    });
  }
});

exports.subscribe = (socket) => {
  sockets[socket.id] = socket;
  console.log(`socket ${socket.id} subscribed`);
};

exports.unsubscribe = (socket) => {
  delete sockets[socket.id];
  console.log(`socket ${socket.id} unsubscribed`);
};

exports.create = async (req, reply) => {
  await create(req.body, req.user.email);
  reply.code(201).send();
};

exports.getPhotosByCenter = async (req, reply) => {
  const { id, centerTimestamp, seconds } = req.params;
  return Promise.all([
    loadPhotoProto(),
    getPhotosByCenter(id, centerTimestamp, seconds)
  ])
  .then(response => {
    const protoModel = response[0];
    const photos = response[1];
    if (!photos || !protoModel) {
      return Promise.reject({message: `no data available.`});
    }
    return buildPhotoProtoBuffer(protoModel, photos);
  })
  .then(responseBuffer => {
    reply
    .code(200)
    .send(responseBuffer);
  })
  .catch(err => {
    reply.send(boom.boomify(err).output);
  });
};

exports.getUploadAccess = async (req, reply) => {
  const data = await getUploadAccess(req.params);
  reply.code(200).send(data);
};

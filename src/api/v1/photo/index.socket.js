const socketConfig = require('../../../config/socket');
const { subscribe, unsubscribe } = require('./photo.controller');
const socketRoutes = [
    {
        event: socketConfig.events.getPhotos,
        subscribe,
        unsubscribe
    }
];

module.exports = socketRoutes;

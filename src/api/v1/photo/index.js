const controller = require('./photo.controller');

const routes = [
    {
        method: 'POST',
        url: '/api/v1/photo',
        handler: controller.create,
        schema: {
            description: 'Takes in data and creates a photo',
            body: {
                type: 'object',
                properties: {
                    deviceId: {
                        type: 'number'
                    },
                    timestamp: {
                        type: 'number'
                    }
                }
            },
            response: {
                201: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/photo/:deviceId/:centerTimestamp/:seconds',
        handler: controller.getPhotosByCenter,
        schema: {
            description: 'Gets photos',
            params: {
                type: 'object',
                properties: {
                    deviceId: {
                        type: 'integer'
                    },
                    centerTimestamp: {
                        type: 'integer'
                    },
                    seconds: {
                        type: 'integer'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            deviceId: {
                                type: 'integer'
                            },
                            timestamp: {
                                type: 'integer'
                            },
                            result: {
                                type: 'string'
                            }
                        }
                    }
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/photo/:filename/token',
        handler: controller.getUploadAccess,
        schema: {
            description: 'Gets S3 access tokens',
            params: {
                type: 'object',
                properties: {
                    filename: {
                        type: 'string'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object',
                    additionalProperties: true
                }
            }
        }
    }
];

module.exports = routes;

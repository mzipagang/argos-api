'use strict';
const nodemailer = require('nodemailer');
const { sender, password, host, port, secure } = require('../../../config').smtp;

const RESET_SENDER_NAME = `"Argos Reset Password" <${sender}>`;
const ACTIVATION_SENDER_NAME = `"Argos Account Activation" <${sender}>`;

const transporter = nodemailer.createTransport({
    host,
    port,
    secure,
    auth: {
        user: sender,
        pass: password
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

const getCustomizedBody = customContent => {
    return '<!DOCTYPE html> \
    <html><head> \
            <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet"> \
            <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> \
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> \
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> \
        </head> \
        <body> \
            <table cellspacing="0" style="max-width:580px;min-width:580px; background-color: #f1f1f1;padding-top: 15px;padding-right: 30px; padding-left: 30px;padding-bottom: 30px;"> \
                <tbody style="vertical-align: top;"> \
                    ' + customContent +
                '</tbody> \
            </table> \
    </body></html>';
};

const sendActivationEmail = ({ email, activateToken, baseUrl }) => {
    const link = baseUrl + '/' + encodeURIComponent(email) + '/' + encodeURIComponent(activateToken);
    const customContent = '<tr style="height: 62px;width: 494px;"> \
                    <td style="line-height: 62px;padding-left: 20px; padding-right:20px; color: #FFF;font-family: \'Nunito Sans\', sans-serif; background-color: #148C99"> \
                        Account Activation \
                    </td> \
                </tr> \
                <tr style="background:#FFF;"> \
                    <td colspan="3" style="padding: 20px;font-family: \'Nunito Sans\', sans-serif;color: #464646;font-size: 14px;"> \
                        To activate your account, please click on the link below. \
                    </td> \
                </tr> \
                <tr style="background:#FFF;"> \
                    <td colspan="3" style="padding:20px;padding-bottom:60px;font-family:&#39;Nunito Sans&#39;,sans-serif;color:#464646;text-align:center"> \
                        <span style="color:#fff;padding-top:10px;background:#e98537;padding-left:20px;padding-right:20px;padding-bottom:10px"> \
                        <a style="text-decoration:none;color:#fff" href="' + link + '">Activate my Account</a> \
                        </span> \
                    </td> \
                </tr>';
    const content = getCustomizedBody(customContent);
    return new Promise((resolve, reject) => {
        transporter.sendMail({
            from: ACTIVATION_SENDER_NAME,
            to: email,
            subject: 'Activate account link',
            html: content,
            body: 'Argos account activation link: ' + link
        }, (error, success) => {
            console.log('ERROR:', error, 'SUCCESS:', success);
            console.log('Message ' + (success ? 'sent' : 'failed'));
            if (success) {
                resolve();  
            } else {
                reject();
            }
        });
    });
};

const sendResetPasswordEmail = ({ email, activateToken, baseUrl }) => {
    const link = baseUrl + '/' + encodeURIComponent(email) + '/' + encodeURIComponent(activateToken);
    const customContent = '<tr style="height: 62px;width: 494px;"> \
                    <td style="line-height: 62px;padding-left: 20px; padding-right:20px; color: #FFF;font-family: \'Nunito Sans\', sans-serif; background-color: #148C99"> \
                        Reset Your Password \
                    </td> \
                </tr> \
                <tr style="background:#FFF;"> \
                    <td colspan="3" style="padding: 20px;font-family: \'Nunito Sans\', sans-serif;color: #464646;font-size: 14px;"> \
                        To reset your password, please click on the link below. \
                    </td> \
                </tr> \
                <tr style="background:#FFF;"> \
                    <td colspan="3" style="padding: 20px;padding-bottom: 60px;font-family: \'Nunito Sans\', sans-serif;color: #464646;text-align: center;"> \
                        <span style=" \
                            color: #FFF; \
                            padding-top: 10px; \
                            background: #e98537; \
                            padding-left: 20px; \
                            padding-right: 20px; \
                            padding-bottom: 10px; \
                        "><a style="text-decoration:none;color:#fff" href="' + link + '">Reset Password</a></span> \
                    </td> \
                </tr>';
    const content = getCustomizedBody(customContent);
    return new Promise((resolve, reject) => {
        transporter.sendMail({
            from: RESET_SENDER_NAME,
            to: email,
            subject: 'Reset password link',
            html: content,
            body: 'Here\'s your reset password link: ' + link
        }, (error, success) => {
            console.log('ERROR:', error, 'SUCCESS:', success);
            console.log('Message ' + (success ? 'sent' : 'failed'));
            if (success) {
                resolve();  
            } else {
                reject();
            }
        });
    });
};

module.exports = {
    sendActivationEmail,
    sendResetPasswordEmail
};

const socketConfig = require('../../../config/socket');
const { subscribe, unsubscribe } = require('./sensordata.controller');
const socketRoutes = [
    {
        event: socketConfig.events.getSensorData,
        subscribe,
        unsubscribe
    }
];

module.exports = socketRoutes;

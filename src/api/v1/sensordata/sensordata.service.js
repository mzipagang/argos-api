const models = require('../../../database/models');
const {SensorData, Sequelize, sequelize} = models;
const { QueryTypes } = require('sequelize');
const {Op} = Sequelize;
const { getPaddedTimes } = require('../../../helpers/helpers');

const latest = async params => {
  const {
      timestamp
  } = params;
  const replacements = { timestamp };
  const data = await sequelize.query(
      `select sd.*,
      (select sd2."siteId" from "SiteDevice" sd2 where sd."deviceId" = sd2."deviceId") as "siteId"
      from "SensorData" sd
      where "timestamp" >= :timestamp
      order by "timestamp" asc`,
      {
          replacements,
          type: QueryTypes.SELECT
      }
  );
  return data;
};

const getSensorDataByCenter = async (deviceId, centerTimestamp, seconds) => {
  const query = {
      where: {
        deviceId
      },
      order: [
          ['timestamp', 'ASC']
      ]
  };

  const paddedTimes = getPaddedTimes(centerTimestamp, seconds);

  query.where.timestamp = {
      [Op.lt]: paddedTimes.end,
      [Op.gte]: paddedTimes.start,
  };

  const data = await SensorData.findAll(query);
  return data;
};

const create = async params => {
    await SensorData.create(params);
};

const buildSensorDataProtoBuffer = (proto, sensorData) => {
  return new Promise(resolve => {
    const SensorDataMessages = proto.lookupType("sensordatapackage.SensorDataMessages");
    const payload = {
      sensorData
    };
    const errMsg = SensorDataMessages.verify(payload);
    if (errMsg) {
      console.log(errMsg);
      throw Error(errMsg);
    }
    const message = SensorDataMessages.create(payload);
    return resolve(SensorDataMessages.encode(message).finish());
  });
};

module.exports = {
    latest,
    create,
    getSensorDataByCenter,
    buildSensorDataProtoBuffer
};

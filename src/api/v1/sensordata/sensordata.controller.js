const boom = require('boom');
const { latest, create, getSensorDataByCenter, buildSensorDataProtoBuffer } = require('./sensordata.service');
const { loadSensorDataProto } = require('../../../../protobuf-models');
const socketConfig = require('../../../config/socket');
const cron = require('node-cron');

const sockets = {};

cron.schedule('*/3 * * * * *', async () => {
  const data = await latest({ timestamp: Math.floor((Date.now() - 5000) / 1000) });
  console.log(`Found ${data.length} sensor data points`);
  if (data && data.length) {
    Object.keys(sockets).forEach(id => {
      const socket = sockets[id];
      return Promise.all([
        loadSensorDataProto(),
        Promise.resolve(data)
      ])
      .then(response => {
        const protoModel = response[0];
        const sensorData = response[1];
        return buildSensorDataProtoBuffer(protoModel, sensorData);
      })
      .then(responseBuffer => {
        return socket.emit(socketConfig.events.getSensorData, responseBuffer);
      })
      .catch(err => {
        console.log(err);
        return socket.emit(socketConfig.events.getSensorData, err.message ? err.message : err);
      });
    });
  }
});

exports.create = async (req, reply) => {
  await create(req.body, req.user.email);
  reply.code(201).send();
};

exports.getSensorDataByCenter = async (req, reply) => {
  const { id, centerTimestamp, seconds } = req.params;
  return Promise.all([
    loadSensorDataProto(),
    getSensorDataByCenter(id, centerTimestamp, seconds)
  ])
  .then(response => {
    const protoModel = response[0];
    const sensorData = response[1];
    if (!sensorData || !protoModel) {
      return Promise.reject({message: `no data available.`});
    }
    return buildSensorDataProtoBuffer(protoModel, sensorData);
  })
  .then(responseBuffer => {
    reply
    .code(200)
    .send(responseBuffer);
  })
  .catch(err => {
    reply.send(boom.boomify(err).output);
  });
};

exports.subscribe = (socket) => {
  sockets[socket.id] = socket;
};

exports.unsubscribe = (socket) => {
  delete sockets[socket.id];
};

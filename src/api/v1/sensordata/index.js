const controller = require('./sensordata.controller');

const routes = [
    {
        method: 'POST',
        url: '/api/v1/sensordata',
        handler: controller.create,
        schema: {
            description: 'Takes in data and creates sensor data',
            body: {
                type: 'object',
                properties: {
                    deviceId: {
                        type: 'number'
                    },
                    timestamp: {
                        type: 'number'
                    },
                    windSpeed: {
                        type: 'number'
                    },
                    windDirection: {
                        type: 'number'
                    },
                    temp: {
                        type: 'number'
                    }
                }
            },
            response: {
                201: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/sensordata/:deviceId/:centerTimestamp/:seconds',
        handler: controller.getSensorDataByCenter,
        schema: {
            description: 'Gets sensor data',
            params: {
                type: 'object',
                properties: {
                    deviceId: {
                        type: 'integer'
                    },
                    centerTimestamp: {
                        type: 'integer'
                    },
                    seconds: {
                        type: 'integer'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            deviceId: {
                                type: 'number'
                            },
                            timestamp: {
                                type: 'number'
                            },
                            windSpeed: {
                                type: 'number'
                            },
                            windDirection: {
                                type: 'number'
                            },
                            temp: {
                                type: 'number'
                            }
                        }
                    }
                }
            }
        }
    }
];

module.exports = routes;

const crypto = require('crypto');

/**
 * Make salt
 *
 * @return {String}
 */
const makeSalt = () => crypto.randomBytes(16).toString('base64');

/**
 * Authenticate - check if the passwords are the same
 *
 * @param {String} password
 * @param {String} hash
 * @return {Boolean}
 */
const isAuthenticated = (password, salt, passwordHash) => {
    const { hash } = encryptPassword(password, salt);
    return passwordHash === hash;
}

/**
 * Encrypt password
 *
 * @param {String} password
 * @return {String}
 */
const encryptPassword = (password, salt) => {
    if (!password) return '';
    salt = salt || makeSalt();
    const hash = crypto.pbkdf2Sync(password, salt, 100000, 64, 'sha512').toString('base64');
    return { hash, salt };
}

module.exports = {
    isAuthenticated,
    encryptPassword
};

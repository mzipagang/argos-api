const boom = require('boom');
const { create, update, get, destroy, search, all } = require('./site.service');

exports.create = async (req, reply) => {
    await create(req.body, req.user.email);
    reply.code(201).send();
};

exports.update = async (req, reply) => {
    const rowsUpdated = await update(req.params.id, req.body, req.user.email);
    if (rowsUpdated && rowsUpdated[0]) {
        reply.code(200).send();
    } else {
        reply.code(404).send();
    }
};

exports.get = async (req, reply) => {
    const event = await get(req.params.id);
    if (event) {
        reply.code(200).send(event);
    } else {
        reply.code(404).send();
    }
};

exports.destroy = async (req, reply) => {
    await destroy(req.params.id, req.user.email);
    reply.code(204).send();
};

exports.all = async (req, reply) => {
    const data = await all(req.params);
    reply.code(200).send(data);
};

exports.search = async (req, reply) => {
    const results = await search(req.body);
    reply.code(200).send(results);
};
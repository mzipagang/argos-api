const models = require('../../../database/models');
const { QueryTypes } = require('sequelize');
const {Site, SiteDevice, Device, Sequelize, sequelize} = models;
const {Op} = Sequelize;

const get = async id => {
    if (!id) {
        return null;
    }
    const data = await Site.findOne({
        where: {
            id
        },
        paranoid: false
    });
    if (data && data.dataValues) {
        const devices = data.dataValues.SiteDevices && data.dataValues.SiteDevices.length ? data.dataValues.SiteDevices.map(f =>
            f.dataValues && f.dataValues.Device ? f.dataValues.Device.dataValues : {}
        ) : [];
        return { ...data.dataValues, devices };
    } else {
        return null;
    }
};

const all = async params => {
    const {
        from = 0,
        limit = 30,
        sortBy,
        sortOrder
    } = params;
    const data = await Site.findAll({
        order: [
            [sortBy || 'createdAt', sortOrder || 'ASC']
        ],
        offset: from,
        limit,
        paranoid: false
    });
    const results = data.map(d => {
        const devices = d.dataValues.SiteDevices && d.dataValues.SiteDevices.length ? d.dataValues.SiteDevices.map(f =>
            f.dataValues && f.dataValues.Device ? f.dataValues.Device.dataValues : {}
        ) : [];
        return { ...d.dataValues, devices };
    });
    return results;
};

const create = async (params, by) => {
    delete params.deletedBy;
    delete params.deletedAt;
    params.createdBy = by;
    params.updatedBy = by;
    await Site.create(params);
};

const update = async (id, params, by) => {
    delete params.id;
    delete params.deletedBy;
    delete params.deletedAt;
    params.updatedBy = by;
    const rowsUpdated = await Site.update(params, {
        where: { id },
        paranoid: false
    });
    return rowsUpdated;
};

const destroy = async (id, by) => {
    try {
        await sequelize.transaction(async t => {
            await Site.update({
                deletedBy: by
            }, {
                where: { id }
            }, { transaction: t });
            await Site.destroy({
                where: { id }
            }, { transaction: t });
            const data = await SiteDevice.findAll({
                where: { siteId: id }
            }, { transaction: t });
            const deviceIds = data.map(d => d.dataValues ? d.dataValues.deviceId : 0);
            await Device.update({
                deletedBy: by
            }, {
                where: { id: deviceIds }
            }, { transaction: t });
            await Device.destroy({
                where: { id: deviceIds }
            }, { transaction: t });
        });
    } catch (err) {
        throw err;
    }
};

const search = async params => {
    const {
        text,
        from = 0,
        limit = 30
    } = params;
    let where = '';
    const replacements = { from, limit };
    if (text) {
        where = `where (cast(s.id as varchar) ilike :text
        or s.name ilike :text
        or s.notes ilike :text
        or s.type ilike :text)`;
        replacements.text = `%${text}%`;
    }
    const sites = await sequelize.query(
        `select s.id, s.name, s."parentSite", s.type, s.lat, s.lng, s.notes
        from "${Site.tableName}" s
        ${where} limit :limit offset :from`,
        {
            replacements,
            type: QueryTypes.SELECT
        }
    );
    return sites;
};

module.exports = {
    get,
    create,
    update,
    all,
    destroy,
    search
};

const controller = require('./site.controller');

const routes = [
    {
        method: 'POST',
        url: '/api/v1/site/search',
        handler: controller.search,
        schema: {
            description: 'Takes in search criteria and returns search results',
            body: {
                type: 'object',
                properties: {
                    text: {
                        type: 'string',
                        description: 'search string'
                    },
                    from: {
                        type: 'integer',
                        description: 'start index of results to return'
                    },
                    limit: {
                        type: 'integer',
                        description: 'max number of results to return'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                type: 'string'
                            },
                            name: {
                                type: 'string'
                            },
                            type: {
                                type: 'string'
                            },
                            notes: {
                                type: 'string'
                            },
                            parentSite: {
                                type: 'string'
                            },
                            lat: {
                                type: 'string'
                            },
                            lng: {
                                type: 'string'
                            },
                            createdAt: {
                                type: 'string'
                            },
                            updatedAt: {
                                type: 'string'
                            },
                            deletedAt: {
                                type: 'string'
                            },
                            createdBy: {
                                type: 'string'
                            },
                            updatedBy: {
                                type: 'string'
                            },
                            deletedBy: {
                                type: 'string'
                            }
                        }
                    }
                }
            }
        }
    },
    {
        method: 'POST',
        url: '/api/v1/site',
        handler: controller.create,
        schema: {
            description: 'Takes in data and creates a site',
            body: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string'
                    },
                    type: {
                        type: 'string'
                    },
                    notes: {
                        type: 'string'
                    },
                    parentSite: {
                        type: 'string'
                    },
                    lat: {
                        type: 'number'
                    },
                    lng: {
                        type: 'number'
                    }
                }
            },
            response: {
                201: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'PUT',
        url: '/api/v1/site/:id',
        handler: controller.update,
        schema: {
            description: 'Takes in an id and data and updates a site',
            params: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        description: 'site id'
                    }
                }
            },
            body: {
                type: 'object',
                properties: {
                    name: {
                        type: 'string'
                    },
                    type: {
                        type: 'string'
                    },
                    notes: {
                        type: 'string'
                    },
                    parentSite: {
                        type: 'string'
                    },
                    lat: {
                        type: 'number'
                    },
                    lng: {
                        type: 'number'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/site/:id',
        handler: controller.get,
        schema: {
            description: 'Gets a site by id',
            params: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'object',
                    properties: {
                        id: {
                            type: 'string'
                        },
                        name: {
                            type: 'string'
                        },
                        type: {
                            type: 'string'
                        },
                        notes: {
                            type: 'string'
                        },
                        parentSite: {
                            type: 'string'
                        },
                        lat: {
                            type: 'number'
                        },
                        lng: {
                            type: 'number'
                        },
                        createdAt: {
                            type: 'string'
                        },
                        updatedAt: {
                            type: 'string'
                        },
                        deletedAt: {
                            type: 'string'
                        },
                        createdBy: {
                            type: 'string'
                        },
                        updatedBy: {
                            type: 'string'
                        },
                        deletedBy: {
                            type: 'string'
                        }
                    }
                }
            }
        }
    },
    {
        method: 'GET',
        url: '/api/v1/site/:from/:limit',
        handler: controller.all,
        schema: {
            description: 'Gets sites',
            params: {
                type: 'object',
                properties: {
                    from: {
                        type: 'integer',
                        description: 'start index of results to return'
                    },
                    limit: {
                        type: 'integer',
                        description: 'max number of results to return'
                    }
                }
            },
            response: {
                200: {
                    description: 'Successful response',
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            id: {
                                type: 'string'
                            },
                            name: {
                                type: 'string'
                            },
                            type: {
                                type: 'string'
                            },
                            notes: {
                                type: 'string'
                            },
                            parentSite: {
                                type: 'string'
                            },
                            lat: {
                                type: 'number'
                            },
                            lng: {
                                type: 'number'
                            },
                            createdAt: {
                                type: 'string'
                            },
                            updatedAt: {
                                type: 'string'
                            },
                            deletedAt: {
                                type: 'string'
                            },
                            createdBy: {
                                type: 'string'
                            },
                            updatedBy: {
                                type: 'string'
                            },
                            deletedBy: {
                                type: 'string'
                            }
                        }
                    }
                }
            }
        }
    },
    {
        method: 'DELETE',
        url: '/api/v1/site/:id',
        handler: controller.destroy,
        schema: {
            description: 'Deletes a site by id',
            params: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number'
                    }
                }
            },
            response: {
                204: {
                    description: 'Successful response',
                    type: 'object'
                }
            }
        }
    }
];

module.exports = routes;

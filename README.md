Requires Node v13.5.0

To install dependencies:

```npm install```

To run locally (using a local postgres db):

```DB_CONNECTION_STRING="postgres://user:password@127.0.0.1:5432/argos" NODE_ENV=development PORT=8000 npm start```
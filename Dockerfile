FROM node:13.5.0
ENV NODE_ENV=development PORT=8000
# Create app directory
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
# for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8000

CMD [ "npm", "start" ]
